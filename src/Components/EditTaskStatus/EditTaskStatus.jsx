import React, { Component } from "react";
import { Input, Row, Col, Select, notification } from "antd";
import { Layout } from "antd";
import qs from "query-string";
import "./editTaskStatus.scss";
import { setTaskTableData } from "../HomePage/homePage.action";
import { connect } from "react-redux";
const { Header, Footer, Content } = Layout;
class EditTaskStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      title: "",
      description: "",
      dueDate: "",
      statusValue: "",
    };
  }
  componentDidMount() {
    let url = this.props.location.search;
    let params = qs.parse(url);
    if (params && params.option) {
      var jsonStr = atob(params.option);
      var searchFilters = JSON.parse(jsonStr);
      this.setState({
        id: searchFilters.id,
        title: searchFilters.title,
        description: searchFilters.description,
        dueDate: searchFilters.dueDate,
        statusValue: searchFilters.status,
      });
    }
  }
  handleStatusChange = (e) => {
    var resulData = this.props.resultTableData;
    var backLogCount = 0;
    var devCount = 0;
    resulData.map((item) => {
      if (item.status == "backlog") {
        backLogCount += 1;
      } else if (item.status == "development") {
        devCount += 1;
      }
    });
    if (e === "backlog" && backLogCount == 5) {
      notification.error({
        message: "Error",
        description: "Maximum task added to Backlog",
      });
      return;
    } else if (e === "developent" && backLogCount == 4) {
      notification.error({
        message: "Error",
        description: "Maximum task added to Development",
      });
      return;
    } else {
      resulData.map((item) => {
        if (item.id === this.state.id) {
          item.status = e;
        }
      });
      this.setState({
        statusValue: e,
      });
      this.props.setTaskTableData(resulData);
      notification.success({
        message: "Success",
        description: "Status Changed Successfully!",
      });
      setTimeout(() => {
        this.props.history.goBack();
      }, 2000);
    }
  };
  render() {
    const { Option } = Select;
    return (
      <Layout>
        <Header style={{ textAlign: "center", color: "white" }}>
          <a href="/"> Task Data </a>
        </Header>
        <Content>
          <Row></Row>
          <Row>
            <Col md={6} className="colVal">
              Title
            </Col>
            <Col md={12}>
              <Input disabled value={this.state.title} />
            </Col>
          </Row>
          <Row>
            <Col md={6} className="colVal">
              Description
            </Col>
            <Col md={12}>
              <Input.TextArea disabled value={this.state.description} />
            </Col>
          </Row>
          <Row>
            <Col md={6} className="colVal">
              Due Date
            </Col>
            <Col md={12}>
              <Input disabled value={this.state.dueDate} />
            </Col>
          </Row>
          <Row>
            <Col md={6} className="colVal">
              Status
            </Col>
            <Col md={12}>
              <Select
                showSearch
                id="status"
                style={{ width: "250px" }}
                value={this.state.statusValue}
                onChange={this.handleStatusChange}
                filterOption={(input, option) => {
                  if (option.props.children != undefined) {
                    return (
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    );
                  }
                }}
              >
                <Option value="backlog">Backlog</Option>
                <Option value="development">Development</Option>
                <Option value="codeReview">Code Review</Option>
                <Option value="acceptance">Acceptance</Option>
              </Select>
            </Col>
          </Row>
        </Content>
        <Footer style={{ textAlign: "center" }}>&copy; footer@copyright</Footer>
      </Layout>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  setTaskTableData: (tableData) => {
    dispatch(setTaskTableData(tableData));
  },
});

const mapStateToProps = (state, ownprop) => {
  return {
    resultTableData: state != undefined ? state.tableData : [],
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditTaskStatus);
