export function setTaskTableData(tableData) {
  return {
    type: "set_task_table_data",
    tableData,
  };
}
