import React, { Component } from "react";
import { Table, Layout, Button, Modal } from "antd";
import "antd/dist/antd.css";
import { Form, Input, DatePicker, notification } from "antd";
import { setTaskTableData } from "./homePage.action";
import { connect } from "react-redux";
import { TableData } from "../../config/constant";
import moment from "moment";
const { Header, Footer, Content } = Layout;
class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heading: "",
      openAddModal: false,
      statusTableData: [],
      dataObject: [],
      titleData: "",
      descriptionData: "",
      dueDateData: "",
    };
  }

  componentDidMount() {
    this.props.setTaskTableData(TableData);
  }

  handleChange = (e, id) => {
    if (id == "titleData" || id == "descriptionData") {
      this.setState({
        [id]: e.target.value,
      });
    } else {
      this.setState({
        [id]: moment(e).format("DD-MMM-YYYY"),
      });
    }
  };

  disablePastDates = (date) => {
    return date < moment().startOf("day");
  };

  fetchStatusData = (e, text) => {
    var statusTableData = [];
    this.props.resultTableData &&
      this.props.resultTableData.map((item) => {
        if (item.status == text) {
          statusTableData.push(item);
        }
      });
    this.setState({
      statusTableData: statusTableData,
      heading:
        text == "backlog"
          ? "Tasks in Backlog"
          : text == "development"
          ? "Tasks in Development"
          : text == "codeReview"
          ? "Task in Code Review"
          : text == "acceptance"
          ? "Task in Acceptance"
          : "",
    });
  };
  addModal = () => {
    var resulData = this.props.resultTableData;
    var backLogCount = 0;
    resulData.map((item) => {
      if (item.status == "backlog") {
        backLogCount += 1;
      }
    });
    if (backLogCount == 5) {
      notification.error({
        message: "Error",
        description: "Cannot Add More Tasks",
      });
      return;
    }
    this.setState({
      openAddModal: !this.state.openAddModal,
    });
  };

  handleOk = () => {
    var resultData = this.props.resultTableData;
    resultData.push({
      id: resultData.length + 1,
      title: this.state.titleData,
      description: this.state.descriptionData,
      dueDate: this.state.dueDateData,
      status: "backlog",
    });
    this.props.setTaskTableData(resultData);

    this.setState({
      titleData: "",
      descriptionData: "",
      dueDateData: "",
      openAddModal: !this.state.openAddModal,
    });
  };

  naviGateToTask = (e, record) => {
    var queryParams =
      "?" +
      new URLSearchParams({
        option: btoa(JSON.stringify(record)),
      });
    this.props.history.push(`/TaskData${queryParams}`);
  };
  render() {
    var resultData = this.props.resultTableData;
    var dataObject = [
      { backlogs: 0, development: 0, codeReview: 0, acceptance: 0 },
    ];
    resultData &&
      resultData.map((item) => {
        if (item.status == "backlog") {
          dataObject[0].backlogs += 1;
        } else if (item.status == "development") {
          dataObject[0].development += 1;
        } else if (item.status == "codeReview") {
          dataObject[0].codeReview += 1;
        } else if (item.status == "acceptance") {
          dataObject[0].acceptance += 1;
        }
      });
    const layout = {
      labelCol: {
        span: 8,
      },
      wrapperCol: {
        span: 16,
      },
    };
    const columns = [
      {
        title: "Backlogs",
        dataIndex: "backlogs",
        key: "backlogs",
        render: (text) => (
          <a onClick={(e) => this.fetchStatusData(e, "backlog")}>{text}</a>
        ),
      },
      {
        title: "Development",
        dataIndex: "development",
        key: "development",
        render: (text) => (
          <a onClick={(e) => this.fetchStatusData(e, "development")}>{text}</a>
        ),
      },
      {
        title: "Code Review",
        dataIndex: "codeReview",
        key: "codeReview",
        render: (text) => (
          <a onClick={(e) => this.fetchStatusData(e, "codeReview")}>{text}</a>
        ),
      },
      {
        title: "Acceptance",
        dataIndex: "acceptance",
        key: "acceptance",
        render: (text) => (
          <a onClick={(e) => this.fetchStatusData(e, "acceptance")}>{text}</a>
        ),
      },
    ];

    const columnsForTaskTable = [
      {
        title: "Title",
        dataIndex: "title",
        key: "title",
        render: (text, record) => (
          <a onClick={(e) => this.naviGateToTask(e, record)}>{text}</a>
        ),
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description",
      },
      {
        title: "Due Date",
        dataIndex: "dueDate",
        key: "dueDate",
      },
    ];

    return (
      <div>
        <Layout>
          <Header style={{ textAlign: "center", color: "white" }}>
            Application Dashboard
          </Header>
          <Content>
            <Button
              style={{ float: "right", margin: "10px" }}
              type="primary"
              onClick={this.addModal}
            >
              Add Task
            </Button>
            <Table
              style={{ padding: "30px" }}
              columns={columns}
              dataSource={dataObject}
              pagination={false}
              bordered
              size="small"
            />
            {this.state.statusTableData &&
              this.state.statusTableData.length > 0 && (
                <div>
                  <h1 style={{ textAlign: "center" }}>{this.state.heading}</h1>
                  <Table
                    style={{ padding: "30px", paddingTop: "0px" }}
                    columns={columnsForTaskTable}
                    dataSource={this.state.statusTableData}
                    pagination={false}
                    bordered
                    size="small"
                  />
                </div>
              )}
          </Content>
          <Footer style={{ textAlign: "center" }}>
            &copy; footer@copyright
          </Footer>
        </Layout>
        <Modal
          title="Add New Task"
          visible={this.state.openAddModal}
          onCancel={this.addModal}
          destroyOnClose={true}
          mask={true}
          maskClosable={false}
          onOk={this.handleOk}
        >
          <Form {...layout}>
            <Form.Item name="Title" label="Title" rules={[{ required: true }]}>
              <Input
                type="text"
                placeholder="Add Title"
                maxLength={30}
                onChange={(e) => this.handleChange(e, "titleData")}
              />
            </Form.Item>
            <Form.Item
              name="Description"
              label="Description"
              rules={[{ required: true }]}
            >
              <Input.TextArea
                placeholder="Add Description"
                type="textarea"
                maxLength={250}
                onChange={(e) => this.handleChange(e, "descriptionData")}
              />
            </Form.Item>
            <Form.Item
              name="Due Date"
              label="Due Date"
              rules={[{ required: true }]}
            >
              <DatePicker
                format="DD-MMM-YYYY"
                onChange={(e) => this.handleChange(e, "dueDateData")}
                disabledDate={this.disablePastDates}
              />
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  setTaskTableData: (tableData) => {
    dispatch(setTaskTableData(tableData));
  },
});

const mapStateToProps = (state, ownprop) => {
  return {
    resultTableData: state != undefined ? state.tableData : [],
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
