export const TableData = [
  {
    id: 1,
    title: "Testing Data",
    description: "Testing URL",
    dueDate: "18-Jul-2020",
    status: "backlog",
  },
  {
    id: 2,
    title: "Testing Data",
    description: "Testing URL",
    dueDate: "18-Jul-2020",
    status: "development",
  },
  {
    id: 3,
    title: "Testing Data",
    description: "Testing URL",
    dueDate: "18-Jul-2020",
    status: "backlog",
  },
  {
    id: 4,
    title: "Testing Data",
    description: "Testing URL",
    dueDate: "18-Jul-2020",
    status: "development",
  },
  {
    id: 5,
    title: "Testing Data",
    description: "Testing URL",
    dueDate: "18-Jul-2020",
    status: "codeReview",
  },
  {
    id: 6,
    title: "Testing Data",
    description: "Testing URL",
    dueDate: "18-Jul-2020",
    status: "acceptance",
  },
];
