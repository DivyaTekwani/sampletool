import React, { Component } from "react";
import { Route } from "react-router-dom";
import "./App.css";
import HomePage from "./Components/HomePage/HomePage";
import EditTaskStatus from "./Components/EditTaskStatus/EditTaskStatus.jsx";

class App extends Component {
  render() {
    return (
      <div>
        <Route path="/" exact component={HomePage} />
        <Route path="/TaskData" component={EditTaskStatus} />
      </div>
    );
  }
}
export default App;
