const initialState = {
  tableData: [],
};

const reducer = function (state = initialState, action) {
  if (action.type === "set_task_table_data") {
    return {
      tableData: action.tableData,
    };
  }
};

export default reducer;
